import Zya from 'https://space-shell.gitlab.io/zya/zya.js'
import PointerEvents from 'https://space-shell.gitlab.io/zya/processes/zya-pointer-events.js'

const Pointer = Zya(PointerEvents)

new Pointer()

// Random left right offset

const textBoxes = document
	.querySelector('.lp-book')
	.querySelectorAll('text-writer')

textBoxes
	.forEach((tw, idx) => {
		const evodd = !!(idx % 2)

		tw.style['transform'] += `translateX(${evodd ? '-' : ''}${(Math.random() * 20) + 10}vw)`
		tw.style['transform'] +=  `rotate(${evodd ? '' : '-'}${Math.random() * 30}deg)`
	})


