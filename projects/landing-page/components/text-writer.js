import Zya from 'https://space-shell.gitlab.io/zya/zya.js'
import Hyper from 'https://cdn.jsdelivr.net/npm/hyperhtml@latest/esm.js'
import PointerEvents from 'https://space-shell.gitlab.io/zya/processes/zya-pointer-events.js'

const css = `
	@keyframes textin {
		from {
			opacity: 0;
			transform: translateX(var(--tw-travel, 2rem));
		}

		to {
			opacity: 1;
		}
	}

	.tw {
		
		&-in {
			display: inline-block;
			animation-duration: var(--tw-speed, 50ms);
			animation-iteration-count: infinite;
			pointer-events: none;
		}

		&-text {

			display: inline;
			pointer-events: none;
		}

		&-start {

			animation-name: textin;
		}
	}
`

less.render(css).then(({ css }) =>
	document.head.insertAdjacentHTML('beforeend', `
		<style id=text-writer>
			${ css }
		</style> `) )

customElements.define('text-writer', Zya(
	class extends HTMLElement {
		constructor () {
			super ({ css })

			const { speed, travel, auto } = this.dataset

			// NOTE - JN - Requires, PointerEvents

			this.text = this.textContent.trim()

			this.textContent = ''

			this.classList.add('tw')

			this.addEventListener('click', () => this.$dispatch({ textWrite: { text: this.text, position: 0, complete: false } }, false))

			speed &&
				this.style.setProperty('--tw-speed', `${speed}ms`)

			travel &&
				this.style.setProperty('--tw-travel', `${travel}rem`)
		}

		connectedCallback () {
			const [ body, character ] = this.render().children

			Object.assign(this, {
				character,
				body
			})
		}

		pointerTarget({ target }) {
			if (target === this)
				this.$dispatch({ textWrite: { text: this.text, complete: false } }, false)
			else
				this.textWritePause()
		}

		textWrite ({ text, position = this.body.textContent.length }) {
			console.log('Writing')

			this.character.style.setProperty('animation-name', 'textin')
			this.character.style.setProperty('animation-iteration-count', 'infinite')

			this.character.addEventListener('animationiteration', evt => {
				position++;

				if (text.length === position)
					this.$dispatch({ textWriteEnd: {} }, false)
				else
					this.render({
						text: text.substring(0, position),
						textin: text[position]
					})
			})
		}


		textWritePause () {
			this.character.style.setProperty('animation-iteration-count', 1)
		}

		textWriteEnd () {
			this.character.style.setProperty('animation-name', 'unset')

			this.render({ text: this.text, complete: true })
		}

		render ({ text, textin, complete = false } = {}) {
			return Hyper.bind(this)`
				<p class=tw-text>${ text }</p>
				<span class=tw-in>${ textin }</span>
				${
					complete
						? ''
						: Hyper.wire()`
							<i>...</i>
						`
				}
			`
		}
	}
))
