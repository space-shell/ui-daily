document.head.append(
	hyperHTML.wire()`
		<style id=password-grid>
			.password-grid {
				display: grid;
				grid-gap: 1rem;
				grid-template-columns: repeat(4, 1fr);
			}

			.password-grid-meter {
				width: 100%;
				height: 2px;
				margin-bottom: 1rem;
			}

			meter::-moz-meter-bar { background: red }
			meter[value="0"]::-moz-meter-bar { background: red }
			meter[value="1"]::-moz-meter-bar { background: khaki }
			meter[value="2"]::-moz-meter-bar { background: orange }
			meter[value="3"]::-moz-meter-bar { background: green }
			meter[value="4"]::-moz-meter-bar { background: green }

			.password-box {
				position: relative;
				padding-top: 100%;
				border: 2px solid var(--clr-pri);
			}

			.password-box:after {
				content: attr(val);
				position: absolute;
				width: 100%;
				top: 50%;
				left: 0;
				transform: translateY(-50%);
			}
		</style>
	`
)

export default (element, input) => {
	const boxesBlank = text => [...Array(16 - text.length)]
		.fill('o')

	const render = (text = '') => hyperHTML.bind(element)`
		<meter class=password-grid-meter value=${zxcvbn(text).score || 0}></meter>
		<div class=password-grid>
			${
				[...text, ...boxesBlank(text)]
					.sort(() => 0.5 - Math.random())
					.map(val =>
						hyperHTML.wire()`
							<div class=password-box val=${val}></div>
						` )
			}
		</div>
	`

	render()

	input.addEventListener('input', ({ target }) => {
		render(target.value)
	})
}
