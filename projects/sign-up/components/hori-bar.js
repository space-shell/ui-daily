const style = document.head.appendChild(
	hyperHTML.wire()`
		<style id=hori-bar>
			.hori-bar-hr {
				top: 0;
				position: absolute;
				transition: transform 500ms ease-in-out;
			}
		</style>
	`
)

export default ({ element = HTMLElement, offset = '1rem', width = '100%', inset = true }) => {
	window.addEventListener('load', evt => {
		style.innerHTML += `
			.hori-bar-hr {
				width: calc(50vw - ${offset} - 100px - 2px);
			}
		`
	})

	element.classList.add('hori-bar')

	const children = [...element.childNodes]
		.filter(n => n.nodeType  === 1)

	const rules = pos => ['left', 'right'].map((rule, idx) =>
		hyperHTML.wire()`
			<hr
				class=hori-bar-hr
				style=${{
					transform: `
						translateX(${idx ? '' : '-'}100%)
						translateY(${pos}px)`,
					[rule]: 0
				}}>
		`
	)

	document.body.addEventListener('click', ({ target }) => {

		if ([...element.childNodes].includes(target) && (target.tagName === 'INPUT' || target.tagName === 'BUTTON'))
			hyperHTML.bind(element)`
				${rules(target.offsetTop + (target.offsetHeight / 2))}
				${children}
			`
			console.log("clicky")
	})
}
