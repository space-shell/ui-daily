import HoriBar from './components/hori-bar.js'
import PasswordGrid from './components/password-grid.js'

HoriBar({
	element: document.body.querySelector('.signup'),
	offset: '1rem',
	width:  '25vw',
	inset: false
})

PasswordGrid(
	document.body.querySelector('password-grid'),
	document.body.querySelector('input.password'),
)

const enterButton = document.body.querySelector('button')
const passwordMeter = document.body.querySelector('meter')

document.body.querySelector('input.password').addEventListener('input', ({ target }) => {
	if (target.value && (Number(passwordMeter.getAttribute('value')) > 2))
		enterButton
			.classList.add('active')
	else
		enterButton
			.classList.remove('active')
})

enterButton.addEventListener('click', evt => {
	location.reload()
})
