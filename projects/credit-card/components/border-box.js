document.head.innerHTML = `
	${document.head.innerHTML}

	<style id=border-box>
		.border-box {
			position: absolute;
			transform-origin: 0 0;
		}
	</style>
`

export default class BorderBox extends HTMLElement {
	constructor () {
		super()

		this.classList.add('border-box')

		this.style.cssText = `
			${this.style.cssText}
			${{
				top: `
					top: 0;
					left: 0;
					transform: translateY(-100%);
				`,
				right: `
					top: 0;
					Right: 0;
					transform: translateX(100%);
				`,
				bottom: `
					bottom: 0;
					right: 0;
					transform: translateY(100%);
				`,
				left: `
					bottom: 0;
					left: 0;
					transform: translateX(-100%);
				`
			}[
				this.getAttribute('border')
				|| 'top'
			]}
		`

	}

	render () {}
}
