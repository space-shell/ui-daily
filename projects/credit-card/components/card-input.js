less.render(`
	.card-input {
		display: inline-block;

		input {
			display: inline-block;
			height: 1rem;
			width: 1rem;
			text-align: center;
		}
	}
`).then(({ css }) =>
	document.head.insertAdjacentHTML('beforeend', `
		<style id=card-input>${ css }</style>
	`) )

export default class CardInput extends HTMLElement {
	constructor () {
		super()

		const { length } = this.dataset

		this.classList.add('card-input')

		const render = () => this.innerHTML =
			[...Array(Number(length) || 16)]
				.fill('')
				.map(n => `<input maxlength=1 pattern=[0-9]+>`)
				.join('')

		this.addEventListener('focus', ({ target }) => {
			target.value = ''
		}, true)

		this.addEventListener('keyup', ({ target }) => {
			if(!target.validity.valid) {
				target.value = ''

				return
			}

			if(target.nextSibling) {
				target.nextSibling.value = ''
				target.nextSibling.focus()
			} else {
				target.blur()
			}
		})

		render()
	}
}

