import BorderBox from './components/border-box.js'
import CardInput from './components/card-input.js'

const strKebab = str => str
	.replace(/([a-z])([A-Z])/g, '$1-$2')
	.replace(/[\s_]+/g, '-')
	.toLowerCase()

;[
	BorderBox,
	CardInput
]
	.forEach(elem => {
		customElements.define(strKebab(elem.name), elem)
	})
