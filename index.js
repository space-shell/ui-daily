const fs = require('fs')

const template = {}

const render = ({ projs }) =>`
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">

		<title>UI Daily</title>

		<style type=text/less>
			body {
				main {
					display: block;
					width: 540px;
					margin: 0  auto;
					text-align: center;
					
					a {

						&:hover {
							text-decoration: none;
						}
					}
				}
			}
		</style>

		<script src=https://cdnjs.cloudflare.com/ajax/libs/less.js/3.7.1/less.min.js></script> 
	</head>

	<body>
		<main>
			${projs}
		</main>
	</body>
</html> `

fs.readdir('projects', (err, dirs) => {
	template.projs = ''

	dirs.forEach(dir => {
		template.projs +=`
			<a href=${dir}>
				<h3>${dir}</h3>
			</a>`
	})

	fs.writeFile('projects/index.html', render(template), err => {
		if(err)
			return console.log(err)
	})
})


